Project overviews
=================

This repository is for keeping track of the entire scope of each individual project. Each markdown file in this repo (other than the readme file you're currently reading) represents a separate project.
